// hi
$fn=10000;

boden();
deckel();

module boden();
{
  
  color("gray")
  {
    difference()
    {
      union()
      {
        translate([0,0,25])
          cylinder(r=72,h=1);
        translate([0,0,2])
          cylinder(r1=69,r2=69.5,h=23);

        translate([0,0,17 ])
        {
          rotate([0,90,0])
            cylinder(r=0.9,h=71);
          rotate([0,90,90])
            cylinder(r=0.9,h=71);
        }
      }
      translate([0,0,0])
        cylinder(r1=67,r2=67,h=23);
      
    translate([0,0,10])
      rotate([0,0,45])
        cube([2, 2*74,20], center=true);
      
    translate([0,0,10])
      rotate([0,0,135])
        cube([2, 2*74,20], center=true);
       
    }
  } 
}

module deckel()
{
  difference()
  {
    color("white")
      cylinder(r=72,h=25);
  
    color("white")
      translate([0,0,1])
        cylinder(r1=69.5,r2=70,h=25);
    
    bajonett();
      rotate([0,0,90])
        bajonett();
  }
}

module bajonett()
{
  color("red") {
    translate([0,0,21])
      cube([2*74,2,10],center=true);
    translate([0,0,17])
      rotate([90,0,-2])
        cube([2*74,2,5],center=true);
    }
}    
