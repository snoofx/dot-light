$fn=1000;

//top_cylinder();
bottom();

module top_cylinder()
{
    color("white") 
    {
    difference()
        {
            cylinder(r=38,h=30);
            translate([0,0,1])
                cylinder(r=37,h=30);
        }
    }
}

module wemos_d1_mini_support() {
    difference() {
        rotate([0,0,90])
            translate([-14,-34,31.6])
                cube([28,35,2.2]);      
        rotate([0,0,90])
            translate([-13,-35,24])
                cube([26,35,10]);  
   
    }
            rotate([0,0,90])
            translate([-14,-4,31.6])
                cube([28,4,2.2]);  
}

 

module bottom_basic()
{
    translate([0,0,20])
        cylinder(r1=37,r2=37.2,h=5);
    translate([0,0,25])
        cylinder(r=38,h=9);
}

module bottom()
{
    color("darkgrey")
    {    
    
        difference()
        {
            bottom_basic();
            
            button_hole();
            translate([0,0,3])
                cylinder(r1=35.8,r2=37,h=30);
            translate([35,-16,26])
                cube([30,32,11]); 

        }
       
        difference()   
        {       
            difference() 
            {
                led_ring_support();
                led_ring_cut(); 
            }
        basic_outer();
    
        }
        wemos_d1_mini_support();

        difference()  {
                    rotate([0,0,90])
                        translate([-14,-35,25])
             cube([28,1,9]);
            usb_hole();
            

            
            
            
            
        }
        wemos_hole();

    }
}



module wemos_hole() {
    
            difference() {
            rotate([0,0,90])
                translate([-14,-45,25])
                    cube([28,10,1]); 
            
            difference() {
                translate([0,0,24])
                    cylinder(r=50,h=3);           
                translate([0,0,24])
                    cylinder(r=38,h=3);
                
        
            }
            
        }   
}

module button_hole()
{
  
    translate([24,20,30])
    rotate([0,90,40])
    cylinder(r=3.6/2,h=100);
}


module usb_hole()
{

            rotate([0,0,90])
                translate([-4,-36,29])
                    cube([8,3,4]); 
}


module basic_outer() {
    difference() {
        translate([0,0,22])
            cylinder(r=38,h=3);
        translate([0,0,22])
            cylinder(r1=36.8,r2=37.2,h=3);
    }
}


module led_ring_cut()
{
color("red"){

translate([0,0,22]){
difference(){
    cylinder(r=65/2,h=2.5);
    translate([0,0,-1])
        cylinder(r=(65/2)-7,h=5.5);

}
}
}
}


module led_ring_support()
{
rotate([0,0,65])
translate([24,-3,23])
cube([13,6,10]); 

rotate([0,0,-45])
translate([24,-3,23])
cube([13,6,10]); 

rotate([0,0,45])
translate([-37,-3,23])
cube([13,6,10]); 

rotate([0,0,-45])
translate([-37,-3,23])
cube([13,6,10]); 
}